<?php 
/**
 * Template for Booking Code Display. 
 *
 */
?>
<div class="cb-headline"><?php echo  __( ' Your Booking Code', 'commons-booking' ); ?></div>
<div class="cb-booking-code cb-box">
  <div class="cb-big cb-booking-code">
    <strong><?php echo $this->b_vars['code'] ; ?></strong>
  </div>
</div>